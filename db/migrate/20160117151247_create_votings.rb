class CreateVotings < ActiveRecord::Migration
  def change
    create_table :votings do |t|
      t.integer :listing_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
