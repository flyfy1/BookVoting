Rails.application.routes.draw do
  root 'listings#index'

  resources :listings do
    post 'vote', on: :member
    post 'unvote', on: :member
  end

  devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" }

end
