class Voting < ActiveRecord::Base
  belongs_to :user
  belongs_to :listing

  validates_uniqueness_of :listing_id, scope: :user

  # TODO: remove duplication in file `listing.rb`
  before_validation { self.user = User.current_user unless self.user}
end
