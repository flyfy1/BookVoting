class Listing < ActiveRecord::Base
  belongs_to :user
  has_many :votings

  acts_as_commentable

  before_validation { self.user = User.current_user unless self.user}

  validates_presence_of :user, :name

  # TODO: thinking there should be a better way
  def vote_from(user)
    Voting.find_by(user:user, listing: self)
  end
end
