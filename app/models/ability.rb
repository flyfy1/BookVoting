class Ability
  include CanCan::Ability

  def initialize(user)
    can :manage, :all

    cannot [:update, :destroy], [Listing, Voting] do |resource|
      if !user
        true
      elsif resource.user == user
        false
      else
        true
      end
    end
  end
end
