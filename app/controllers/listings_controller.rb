class ListingsController < BaseController

  before_action :authenticate_user!, :except => [:index]

  def index
    @listings = Listing.all
  end

  def show
  end

  def new
    @listing = Listing.new
  end

  def edit
  end

  def create
    @listing = Listing.new(listing_params)

    respond_to do |format|
      if @listing.save
        format.html { redirect_to @listing, notice: 'Listing was successfully created.' }
        format.json { render :show, status: :created, location: @listing }
      else
        format.html { render :new }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @listing.update(listing_params)
        format.html { redirect_to @listing, notice: 'Listing was successfully updated.' }
        format.json { render :show, status: :ok, location: @listing }
      else
        format.html { render :edit }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @listing.destroy
    respond_to do |format|
      format.html { redirect_to listings_url, notice: 'Listing was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # This method only handle pjax request
  def vote
    @voting = @listing.votings.build
    render_row_response {@voting.save}
  end

  def unvote
    @voting = @listing.vote_from(current_user)
    render_row_response {@voting.destroy}
  end

  private
    def render_row_response(&blk)
      respond_to do |f|
        if blk.call
          @replace = "listing-row-#{@listing.id}"
          @layout = 'row'
          f.html { redirect_to listing_path(@listing)}
          f.js { render 'replace_partial.js.erb' }
        else
          f.js { render js: 'alert("It does not work.. Tears.")' }
        end
      end
    end

    def listing_params
      params.require(:listing).permit(:name, :url, :description, :user_id)
    end
end
